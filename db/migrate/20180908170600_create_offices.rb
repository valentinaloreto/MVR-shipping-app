class CreateOffices < ActiveRecord::Migration[5.2]
  def change
    create_table :offices do |t|
      t.string :phone
      t.string :address
      t.string :email
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
