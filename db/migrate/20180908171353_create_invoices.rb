class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.references :shipment, foreign_key: true
      t.decimal :initial_total
      t.decimal :paid

      t.timestamps
    end
  end
end
