class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.decimal :price
      t.date :date
      t.date :estimate_pickup
      t.references :shipment, foreign_key: true

      t.references :origin, foreign_key: {to_table: :offices}, null: false
      t.references :destination, foreign_key: {to_table: :offices}, null: false

      t.timestamps
    end
  end
end
