class CreateFreights < ActiveRecord::Migration[5.2]
  def change
    create_table :freights do |t|
      t.text :description
      t.integer :kind
      t.decimal :weight
      t.references :shipment, foreign_key: true

      t.timestamps
    end
  end
end
