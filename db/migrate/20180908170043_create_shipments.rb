class CreateShipments < ActiveRecord::Migration[5.2]
  def change
    create_table :shipments do |t|
      t.integer :status
      t.references :recipient, foreign_key: {to_table: :clients}, null: false
      t.references :sender, foreign_key: {to_table: :clients}, null: false

      t.timestamps
    end
  end
end
