# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_08_171353) do

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.integer "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_cities_on_state_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "freights", force: :cascade do |t|
    t.text "description"
    t.integer "kind"
    t.decimal "weight"
    t.integer "shipment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shipment_id"], name: "index_freights_on_shipment_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "shipment_id"
    t.decimal "initial_total"
    t.decimal "paid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shipment_id"], name: "index_invoices_on_shipment_id"
  end

  create_table "offices", force: :cascade do |t|
    t.string "phone"
    t.string "address"
    t.string "email"
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_offices_on_city_id"
  end

  create_table "routes", force: :cascade do |t|
    t.decimal "price"
    t.date "date"
    t.date "estimate_pickup"
    t.integer "shipment_id"
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["destination_id"], name: "index_routes_on_destination_id"
    t.index ["origin_id"], name: "index_routes_on_origin_id"
    t.index ["shipment_id"], name: "index_routes_on_shipment_id"
  end

  create_table "shipments", force: :cascade do |t|
    t.integer "status"
    t.integer "recipient_id", null: false
    t.integer "sender_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipient_id"], name: "index_shipments_on_recipient_id"
    t.index ["sender_id"], name: "index_shipments_on_sender_id"
  end

  create_table "states", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
