# README

Reto para hacer en clase

Implementar el sistema (API SOA) de control para la empresa de envío express de encomiendas MVR que, a nivel nacional, ofrece los siguientes servicios:
  envíos prepagados de documentos
  envíos prepagados de paquetes (hasta 5kg de peso)


Proceso de envío de encomienda:

  Un cliente acude a una sede de MVR con el paquete
  Se presupuesta el paquete o documento (tipo, descripción, peso, sede de MVR destino, datos del destinatario).
  Si el cliente aprueba el presupuesto, cancela el costo del envío.
  Se busca al cliente en el sistema.
  Si no existe, se registra el cliente.
  Se aprueba el envío ya cancelado: se registra en el sistema.
  Cuando el paquete llega a la sede destino y el destinatario acude a retirarlo, debe indicar el N° de envío para que su paquete pueda ser encontrado dentro de la sede y le sea entregado. La entrega del paquete al destinatario debe registrarse en el envío.
  Es posible que el cliente remitente cambie la sede de destino del paquete para que el receptor pueda retirarlo en esta última, lo que genera un cargo adicional.


Requerimientos:

  Registrar un cliente remitente para que pueda enviar encomiendas
  Registrar la orden de envío de encomiendas a otra sede de MVR.
  Registrar la recepción (llegada) de una encomienda en una sede.
  Registrar la entrega de una encomienda al receptor final.
  Verificar estado de un envío.
  Ver envíos pendientes por despachar.
  Ver envíos pendientes por entregar.
  Registrar la redirección de una sede de MVR a otra de una encomienda a petición del cliente remitente.
  Cancelar los cargos adicionales generados por los cambios de sede de destino.

----------------------------------------------------------

  created project, added serializer gem to GemFile, bundle intalled it
  modeled the db in dia
  generated the models and corresponding migrations
  customized associations in the models
  seeded the tables that needed to be filled --offices, cities, states
  tinkered with the db and ActiveRecord methods via the console (throught the rails c command)
  defined the routes in routes.db
  created the controllers
  customized the controllers
  mix of exchange between Postman, rails console, adding code to controllers
  finally, generated the serializer for client model but it didn't work out. not sure why
