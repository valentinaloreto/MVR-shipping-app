class ClientsController < ApplicationController
  def index
    @clients = Client.all
    render json: !@clients.empty? ? @clients : { error: "No clients in record "}
  end

  def show
    @client = find_client( params[:id] )
    render json: @client
  end

  def create
    @client = Client.create( client_params )
    render json: @client
  end

  def update
    @client = find_client( params[:id] )
    if @client.kind_of?(Client)
      @client.update!( client_params )
      render json: @client
    else
      render json: @client
    end
  end

  def destroy
    @client = find_client( params[:id] )
    if @client.kind_of?(Client)
      @client.destroy
      render json: { message: "Client deleted" }
    else
      render json: @client
    end
  end

  private

  def client_params
    params.permit(:name, :email, :phone)
  end

  def find_client(id)
    begin
      Client.find(id)
    rescue => e
      return { error: e.message }
    end
  end

end
