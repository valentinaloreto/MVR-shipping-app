class RoutesController < ApplicationController
  def index
    @routes = Route.all
    render json: !@routes.empty? ? @routes : { error: "No routes in record "}
  end

  def show
    @route = find_route( params[:id] )
    render json: @route
  end

  def create
    @route = Route.create( route_params )
    render json: @route
  end

  def update
    @route = find_route( params[:id] )
    if @route.kind_of?(Route)
      @route.update!( route_params )
      render json: @route
    else
      render json: @route
    end
  end

  def destroy
    @route = find_route( params[:id] )
    if @route.kind_of?(Route)
      @route.destroy
      render json: { message: "route deleted" }
    else
      render json: @route
    end
  end

  private

  def route_params
    params.permit(:price, :shipment_id, :origin_id, :destination_id, :date, :estimate_pickup)
  end

  def find_route(id)
    begin
      Route.find(id)
    rescue => e
      return { error: e.message }
    end
  end
end
