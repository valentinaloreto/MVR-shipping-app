class InvoicesController < ApplicationController
  def index
    @invoices = Invoice.all
    render json: !@invoices.empty? ? @invoices : { error: "No invoices in record "}
  end

  def show
    @invoice = find_invoice( params[:id] )
    render json: @invoice
  end

  def create
    @invoice = Invoice.create( invoice_params )
    render json: @invoice
  end

  def update
    @invoice = find_invoice( params[:id] )
    if @invoice.kind_of?(Invoice)
      @invoice.update!( invoice_params )
      render json: @invoice
    else
      render json: @invoice
    end
  end

  def destroy
    @invoice = find_invoice( params[:id] )
    if @invoice.kind_of?(Invoice)
      @invoice.destroy
      render json: { message: "invoice deleted" }
    else
      render json: @invoice
    end
  end

  private

  def invoice_params
    params.permit(:shipment_id, :initial_total, :paid)
  end

  def find_invoice(id)
    begin
      Invoice.find(id)
    rescue => e
      return { error: e.message }
    end
  end
end
