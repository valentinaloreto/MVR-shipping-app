class ShipmentsController < ApplicationController
  def index
    @shipments = Shipment.all
    render json: !@shipments.empty? ? @shipments : { error: "No shipments in record "}
  end

  def show
    @shipment = find_shipment( params[:id] )
    render json: @shipment
  end

  def create
    @shipment = Shipment.create( shipment_params )
    render json: @shipment
  end

  def update
    @shipment = find_shipment( params[:id] )
    if @shipment.kind_of?(Shipment)
      @shipment.update!( shipment_params )
      render json: @shipment
    else
      render json: @shipment
    end
  end

  def destroy
    @shipment = find_shipment( params[:id] )
    if @shipment.kind_of?(Shipment)
      @shipment.destroy
      render json: { message: "Shipment deleted" }
    else
      render json: @shipment
    end
  end

  private

  def shipment_params
    params.permit(:status, :sender_id, :recipient_id)
  end

  def find_shipment(id)
    begin
      Shipment.find(id)
    rescue => e
      return { error: e.message }
    end
  end
end
