class FreightsController < ApplicationController
  def index


  end

  def show
    @freight = find_freight( params[:id] )
    render json: @freight
  end

  def create
    @freight = Freight.create( freight_params )
    render json: @freight
  end

  def update
    @freight = find_freight( params[:id] )
    if @freight.kind_of?(Freight)
      @freight.update!( freight_params )
      render json: @freight
    else
      render json: @freight
    end
  end

  def destroy
    @freight = find_freight( params[:id] )
    if @freight.kind_of?(Freight)
      @freight.destroy
      render json: { message: "freight deleted" }
    else
      render json: @freight
    end
  end

  private

  def freight_params
    params.permit(:description, :weight, :kind, :shipment_id)
  end

  def find_freight(id)
    begin
      Freight.find(id)
    rescue => e
      return { error: e.message }
    end
  end
end
