class Shipment < ApplicationRecord
  belongs_to :recipient, foreign_key: :recipient_id, class_name: 'Client'
  belongs_to :sender, foreign_key: :sender_id, class_name: 'Client'
  has_one :freight
  has_many :routes
  has_many :invoices

  enum status: [:pending, :sent, :received, :delivered]
end
