class Freight < ApplicationRecord
  belongs_to :shipment

  enum kind: [:package, :document]
end
