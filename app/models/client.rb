class Client < ApplicationRecord
  has_many :sent_shipments, foreign_key: :sender_id, class_name: 'Shipping'
  has_many :received_shipments, foreign_key: :recipient_id, class_name: 'Shipping'
end
