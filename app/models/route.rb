class Route < ApplicationRecord
  belongs_to :shipment

  belongs_to :origin, class_name: 'Office', foreign_key: :origin_id
  belongs_to :destination, class_name: 'Office', foreign_key: :destination_id

end
