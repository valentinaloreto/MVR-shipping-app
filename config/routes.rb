Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :clients
  resources :shipments
  resources :freights, except: :index
  resources :routes
  resources :invoices
end
